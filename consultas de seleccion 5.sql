﻿-- 1nombre y edad de los ciclistas que no han ganado etapas .
  SELECT nombre,edad fROM ciclista;
  SELECT * FROM etapa;
  SELECT nombre,edad FROM ciclista LEFT JOIN (SELECT * FROM etapa WHERE dorsal)c1 USING (dorsal) WHERE c1.dorsal IS NULL;

-- 2 nombre y edad de los ciclistas que no han ganado puertos .
  SELECT * FROM puerto;
  SELECT nombre,edad FROM ciclista LEFT JOIN (SELECT * FROM puerto) c1 USING (dorsal) WHERE c1.dorsal IS NULL;

 -- 3 listar el director de los equopos que tengan ciclistas que NO hayan ganado Ninguna etapa .
  SELECT director FROM equipo LEFT JOIN(SELECT * FROM etapa) c1 ON equipo.director=c1.dorsal WHERE c1.dorsal IS NULL;
  
  -- 4 dorsal y nombre de los ciclistas que no hayan llebado algun maillot.
    SELECT ciclista.dorsal,ciclista.nombre FROM  ciclista LEFT JOIN (SELECT * FROM maillot) c1 ON ciclista.dorsal=c1.código WHERE c1.código IS NULL;

 -- 5 dorsal y nombre de los ciclistas que no hayan llevado el maillot amarillo NUNCA 
   SELECT ciclista.dorsal, ciclista.nombre FROM ciclista LEFT JOIN (SELECT * FROM maillot)c1 ON ciclista.dorsal=c1.color WHERE c1.color='amarillo' IS null;

-- 6 Indicar el numero de etapas que no tengan puertos
  SELECT COUNt(e.numetapa) FROM etapa e  LEFT JOIN (SELECT * FROM puerto) c1 ON e.dorsal=c1.dorsal WHERE c1.dorsal IS NULL;

-- 7 indicar la distancia media de las etapas que no tengan puertos 
   SELECT AVG(e.numetapa) FROM etapa e  LEFT JOIN (SELECT * FROM puerto) c1 ON e.dorsal=c1.dorsal WHERE c1.dorsal IS NULL;

-- 8 Listar el numero de ciclistas que No hayan ganado alguna etapa
  SELECT COUNT(c.dorsal) FROM ciclista c LEFT JOIN (SELECT * FROM etapa) c1 ON c.dorsal=c1.dorsal WHERE c1.dorsal IS NULL; 

-- 9 listar el dorsal de los ciclistas que hayan ganado alguna etapa que no tenga puerto
  SELECT * FROM ciclista c1 NATURAL JOIN etapa; c1 
  SELECT puerto.dorsal FROM  puerto LEFT JOIN ( SELECT * FROM ciclista c1 NATURAL JOIN etapa) c1 ON puerto.dorsal=c1.dorsal WHERE puerto.dorsal IS NULL;

  SELECT DISTINCT etapa.dorsal FROM etapa LEFT JOIN puerto ON etapa.numetapa=puerto.numetapa WHERE puerto.numetapa IS NULL;

-- 10 listar el dorsal de los ciclistas que hayan ganado únicamente etapas que no tengan puerto
  SELECT DISTINCT e.dorsal FROM etapa e INNER JOIN puerto p ON e.numetapa=p.numetapa; -- dorsal de ciclistas que han ganado etapas con puerto
  SELECT DISTINCT ciclista.dorsal FROM ciclista 
  LEFT JOIN (SELECT DISTINCT e.dorsal FROM etapa e INNER JOIN puerto p ON e.numetapa=p.numetapa) c1 
  ON ciclista.dorsal=c1.dorsal WHERE c1.dorsal IS NULL; -- dorsal de los ciclistas de las etapas que no tienen puerto.


